# gencodegouv

Generate static HTML pages from [Open Software Base](https://git.framasoft.org/etalab/open-software-base-yaml).

## Install

Download [this program](https://git.framasoft.org/etalab/gencodegouv):
```bash
git clone https://git.framasoft.org/etalab/gencodegouv.git
```

Install dependencies:
```bash
npm install
```

The [Open Software Base (in YAML format)](https://git.framasoft.org/etalab/open-software-base-yaml) is included in the dependencies.

## Usage

```bash
cd gencodegouv/
npm run build:html
npm run build:styles
npm run build:javascript
```

## Development

In development, you can watch directories to instantly rebuild pages and stylesheets upon modification.

To monitor templates:
```bash
npm run build:html:watch
```

To monitor stylesheets:
```bash
npm run build:styles:watch
```

To monitor javascript:
```bash
npm run build:javascript:watch
```
