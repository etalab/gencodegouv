const webpack = require("webpack");

module.exports = {
  devtool: 'cheap-module-source-map',
  entry: './js',
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  output: {
    path: './html/',
    filename: 'bundle.js',
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        loaders: 'babel-loader',
        query: {
          presets: ['react']
        }
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({'process.env': {
      'NODE_ENV': JSON.stringify('production')
    }})
  ]
}
