#! /usr/bin/env python3


# gencodegouv -- Part of codegouv project - generates static HTML files from YAML data
# By: Marine Gout <marine.gout@data.gouv.fr>
#     Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
#
# Copyright (C) 2015 Etalab
# https://git.framasoft.org/etalab/gencodegouv
#
# gencodegouv is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# gencodegouv is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http:>www.gnu.org/licenses/>.


import argparse
import logging
import os
import sys
import time

from mako.lookup import TemplateLookup
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import yaml


args = None
languages = ['en', 'es', 'fr']
software_template = None

class ChangeHandler(FileSystemEventHandler):
    def __init__(self, callback):
        if callable(callback):
            self.callback = callback
        else:
            raise TypeError('callback is required')
    def on_any_event(self, event):
        print('Files changed, rebuild...')
        self.callback()

def generate(yaml_path):
    with open(yaml_path) as yaml_file:
        software = yaml.load(yaml_file)
        if 'debian' in software:
            for language in languages:
                with open(os.path.join(args.target_dir, language, "{}.html".format(software['name'])),
                        'w') as html_file:
                    html_file.write(software_template.render(
                        language = language,
                        software = software,
                        ))
    return software


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('source_dir', help = 'path of source directory (YAML files)')
    parser.add_argument('target_dir', help = 'path of target directory (HTML files)')
    parser.add_argument('-s', '--software', help = 'name of single software page to generate')
    parser.add_argument('-v', '--verbose', action = 'store_true', default = False, help = 'increase output verbosity')
    parser.add_argument('-w', '--watch', action = 'store_true', default = False, help = 'watch for changes in directory')
    global args
    args = parser.parse_args()
    logging.basicConfig(level = logging.DEBUG if args.verbose else logging.WARNING, stream = sys.stdout)

    assert os.path.exists(args.source_dir), "Source path doesn't exist: {}.".format(args.source_dir)
    if not os.path.exists(args.target_dir):
        os.mkdir(args.target_dir)
    for language in languages:
        language_dir = os.path.join(args.target_dir, language)
        if not os.path.exists(language_dir):
            os.mkdir(language_dir)

    if args.watch:
        event_handler = ChangeHandler(build)
        observer = Observer()
        observer.schedule(event_handler, 'templates', recursive=True)
        observer.start()
        try:
            while True:
                time.sleep(1)
        except KeyboardInterrupt:
            observer.stop()
        observer.join()
    else:
        build()

    return 0

def build():
    lookup = TemplateLookup(
        default_filters = ['h'],
        directories = ['templates'],
        encoding_errors = 'strict',
        input_encoding = 'utf-8',
        module_directory = 'cache',
        )
    global software_template
    software_template = lookup.get_template('/software.html')

    if args.software:
        generate(os.path.join(args.source_dir, "{}.yaml".format(args.software)))
    else:
        # Generate software pages.
        software_names = []
        software_images = {}
        for dir, dir_names, filenames in os.walk(args.source_dir):
            for name in dir_names[:]:
                if name.startswith('.'):
                    dir_names.remove(name)
            for filename in filenames:
                if not filename.endswith('.yaml'):
                    continue
                software = generate(os.path.join(dir, filename))
                if 'debian' in software:
                    software_names.append(software['name'])
                    if 'screenshot' in software['debian']:
                        software_images[software['name']] = software['debian']['screenshot']['small_image_url']

        # Generate index pages.
        index_template = lookup.get_template('/index.html')
        for language in languages:
            with open(os.path.join(args.target_dir, language, "index.html"), 'w') as html_file:
                html_file.write(index_template.render(
                    language = language,
                    software_names = software_names,
                    software_images = software_images
                    ))
    print('Build successful.')
    return 0


if __name__ == "__main__":
    sys.exit(main())
