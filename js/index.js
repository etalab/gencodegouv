const React = require('react');
const ReactDOM = require('react-dom');

var Dropdown = React.createClass({
  getInitialState: function() {
    return {
        listVisible: false
    };
  },

  show: function() {
    this.setState({listVisible: true});
    document.addEventListener('click', this.hide);
  },

  hide: function() {
    this.setState({ listVisible: false });
    document.removeEventListener('click', this.hide);
  },

  render: function() {
    var displayValue = (this.state.listVisible ? 'block':'none');
    return <div className="dropdown-container">
      <a href="#" className={'dropdown-display' + (this.state.listVisible ? ' clicked': '')} onClick={this.show}>
        {'Langue'} <i className="fa fa-angle-down"></i>
      </a>
      <ul className="list-reset dropdown-menu" style={{display: displayValue}}>
        {this.renderListItems()}
      </ul>
    </div>;
  },

  renderListItems: function() {
    var items = [];
    for (var i = 0; i < this.props.list.length; i++) {
      var item = this.props.list[i];
      items.push(<li><a href={item.href}>{item.name}</a></li>);
    }
    return items;
  }
});

var links = [{
  name: 'English',
  href: '/en/'
}, {
  name: 'Français',
  href: '/fr/'
}, {
  name: 'Espanol',
  href: '/es/'
}];

ReactDOM.render(<Dropdown list={links} selected={links[0]} />, document.getElementById('languageMenu'));
